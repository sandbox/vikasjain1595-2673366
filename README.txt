Payment Module : Commerce Mobikwik Wallet India Payment Gateway
***********************************************************

Mobikwik Wallet:
 
Adding money to your wallet lets you pay for your services in just one click.        
                
************************************************************

This module is for the Drupal Commerce module for Drupal. 
(Drupal Commerce may be installed as an additional module) 
                       or 
may have been installed as a part of the Commerce Kickstart package.
(which includes the Drupal Core Version as well)
 
Please find below the installation procedure for the module:

- Put the module folder inside module directory of your project.
 (sites/all/modules) 
- Go to drupal admin module page (admin/modules).
- Enable the module on the Modules Page.
- Navigate to 'Payment Methods' Under 'Store'/'Store Settings' using Admin.
- Enable Mobikwik WALLET as a payment method.
- Click on Edit under Operations corresponding to Mobikwik Wallet.
- Click on Edit under Operations 
     corresponding to Actions 
     corresponding to Mobikwik Wallet.
- Enter your Mobikwik Wallet Merchant id and Secret key.
  For Test Mode:
    Default Merchant id is 'MBK9002'
    Default Secret Key is 'ju6tygh7u7tdg554k098ujd5468o'
	Default Gateway URL is 'https://test.mobikwik.com/wallet'
	Default Transaction URL is 'https://test.mobikwik.com/checkstatus'.
	
- Click on Save.

Note: To get live Mobikwik wallet Merchant Id and Secret Key, 

- Go to your Mobikwik Merchant Panel at http://wallet.mobikwik.com
- Login with your credentials, click on Technical Confirguration tab on the top
  (This link is visible only if your account is activated,
   Till then use test mode for testing of Mobikwik Wallet on your website). 
- Click on Step 2 to see your live Merchant ID and secret key.
