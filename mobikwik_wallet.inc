<?php

/**
 * @file
 * Class that will handle the Payment with Mobikwik.
 */

/**
 * Class for handling payment.
 */
class MobikwikWallet {
  protected $mid;
  protected $secret;
  protected $mode;
  protected $gatewayUrl;
  protected $transactionUrl;

  /**
   * Constructor.
   *
   * @param string $settings
   *   Set settings variables.
   */
  public function __construct($settings) {
    if ($settings['mobikwik_wallet_production']) {
      $settings = array(
        'mid' => $settings['production']['mobikwik_wallet_mid'],
        'secret_key' => $settings['production']['mobikwik_wallet_secret'],
        'gateway_url' => $settings['production']['mobikwik_wallet_gatewayurl'],
        'transaction_url' => $settings['production']['mobikwik_wallet_trnxurl'],
        'mode' => 1,
      );
    }
    else {
      $settings = array(
        'mid' => $settings['sandbox']['mobikwik_wallet_sand_mid'],
        'secret_key' => $settings['sandbox']['mobikwik_wallet_sand_secret'],
        'gateway_url' => $settings['sandbox']['mobikwik_wallet_sand_gatewayurl'],
        'transaction_url' => $settings['sandbox']['mobikwik_wallet_sand_trnxurl'],
        'mode' => 0,
      );
    }
    $this->setMode($settings['mode']);
    $this->setMid($settings['mid']);
    $this->setSecret($settings['secret_key']);
    $this->setGatewayUrl($settings['gateway_url']);
    $this->setTnxUrl($settings['transaction_url']);
  }

  /**
   * Setter Method.
   *
   * @param string $transaction_url
   *   Set transactionUrl.
   */
  public function setTnxUrl($transaction_url) {
    $this->transactionUrl = $transaction_url;
  }

  /**
   * Get transactionUrl.
   */
  public function getTnxUrl() {
    return $this->transactionUrl;
  }

  /**
   * Setter Method.
   *
   * @param string $gateway_url
   *   Set gatewayUrl.
   */
  public function setGatewayUrl($gateway_url) {
    $this->gatewayUrl = $gateway_url;
  }

  /**
   * Get gatewayUrl.
   */
  public function getGatewayUrl() {
    return $this->gatewayUrl;
  }

  /**
   * Setter Method.
   *
   * @param string $mode
   *   Set mode.
   */
  public function setMode($mode) {
    $this->mode = $mode;
  }

  /**
   * Get mode.
   */
  public function getMode() {
    return $this->mode;
  }

  /**
   * Setter Method.
   *
   * @param string $mid
   *   Set mid.
   */
  public function setMid($mid) {
    $this->mid = $mid;
  }

  /**
   * Setter Method.
   *
   * @param string $secret
   *   Set secret.
   */
  public function setSecret($secret) {
    $this->secret = $secret;
  }

  /**
   * Get mid.
   */
  public function getMid() {
    return $this->mid;
  }

  /**
   * Get secret.
   */
  public function getSecret() {
    return $this->secret;
  }

}
